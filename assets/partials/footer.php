<footer>
	<div class="container">
		<ul class="hyper-icon-row">
			<li>
				<a class="tooltip" href="#" title="Facebook">
					<span class="icon"><?php include 'assets/images/icons/facebook.svg'; ?></span>
				</a>
			</li>	
			<li>
				<a class="tooltip" href="#" title="Twitter">
					<span class="icon"><?php include 'assets/images/icons/twitter.svg'; ?></span>
				</a>
			</li>	
			<li>
				<a class="tooltip" href="#" title="Pinterest">
					<span class="icon"><?php include 'assets/images/icons/pinterest.svg'; ?></span>
				</a>
			</li>	
			<li>
				<a class="tooltip" href="#" title="Pinterest">
					<span class="icon"><?php include 'assets/images/icons/pinterest-alt.svg'; ?></span>
				</a>
			</li>	
			<li>
				<a class="tooltip" href="#" title="Instagram">
					<span class="icon"><?php include 'assets/images/icons/instagram.svg'; ?></span>
				</a>
			</li>	
			<li>
				<a class="tooltip" href="#" title="Vimeo">
					<span class="icon"><?php include 'assets/images/icons/vimeo.svg'; ?></span>
				</a>
			</li>	
			<li>
				<a class="tooltip" href="#" title="Dribbble">
					<span class="icon"><?php include 'assets/images/icons/dribbble.svg'; ?></span>
				</a>
			</li>	
			<li>
				<a class="tooltip" href="#" title="Youtube">
					<span class="icon"><?php include 'assets/images/icons/youtube.svg'; ?></span>
				</a>
			</li>	
			<li>
				<a class="tooltip" href="#" title="Google+">
					<span class="icon"><?php include 'assets/images/icons/google-plus.svg'; ?></span>
				</a>
			</li>	
			<li>
				<a class="tooltip" href="#" title="Flickr">
					<span class="icon"><?php include 'assets/images/icons/flickr.svg'; ?></span>
				</a>
			</li>	
			<li>
				<a class="tooltip" href="#" title="LinkedIn">
					<span class="icon"><?php include 'assets/images/icons/linkedin.svg'; ?></span>
				</a>
			</li>	
			<li>
				<a class="tooltip" href="#" title="Email">
					<span class="icon"><?php include 'assets/images/icons/email.svg'; ?></span>
				</a>
			</li>
		</ul>
		<h4>Newsletter</h4>
      	<form id="newsletterForm">
      		<div class="inline-grid">
      			<div class="col-8">
		        	<input type="text" name="e_mail_address" placeholder="Email Address"/>
	        	</div>
	        	<div class="col-4">
		        	<button type="submit" class="medium" id="newsletterSubmit"/>Sign Up</button>
		        </div>
        	</div>
      	</form>
	</div>
</footer>