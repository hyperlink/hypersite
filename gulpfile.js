'use strict';

var gulp        = require('gulp'),
    sass        = require('gulp-sass'),
    notify      = require('gulp-notify'),
    sourcemaps  = require('gulp-sourcemaps'),
    gutil       = require('gulp-util'),
    uglify      = require('gulp-uglify'),
    concat      = require('gulp-concat'),
    browserify  = require('browserify'),
    source      = require('vinyl-source-stream'),
    buffer      = require('vinyl-buffer'),
    destPath    = 'assets/';

gulp.task('styles', function() {
  var input = './src/scss/main.scss',
      output = 'assets/css',
      options = {
        errLogToConsole: true,
        outputStyle: 'expanded'
      };
  return gulp
  .src(input)
  .pipe(sourcemaps.init())
  .pipe(sass(options).on('error', sass.logError))
  .pipe(sourcemaps.write('.'))
  .pipe(gulp.dest(output))
  .pipe(notify({message: "Styled", onLast: true}));
});


gulp.task('scripts', function() {
  return browserify('./src/js/script.js',{debug: true})
  .bundle()
  .pipe(source('script.js'))
  .pipe(buffer())
  // .pipe(sourcemaps.init({loadMaps: true}))
  //     .pipe(uglify())
  //     .on('error', gutil.log)
  // .pipe(sourcemaps.write('./'))
  .pipe(gulp.dest(destPath + 'js/'))
  .pipe(notify({message: "Scripted", onLast: true}));
});

gulp.task('watch', function() {
  gulp.watch('./src/scss/*.scss', ['styles']);
  gulp.watch('./src/js/*.js', ['scripts']);
});

gulp.task('build', ['styles', 'scripts']);
gulp.task('default', ['styles','scripts','watch']);