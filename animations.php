<?php include './assets/partials/head.php'; ?>
<body>
	<div id="page">
		<?php include './assets/partials/header.php'; ?>

		<div id="content">
			<div class="container">
			<?php //include 'assets/includes/hyper-components/accordion.php'; ?>
				<h1>Animations</h1>
				<h6>Hyperlink Globe <span class="light">— A div with a class of <code>.hyperglobe</code> with 21 empty <code>span</code></span></h6>
				<div class="hyper-animation">
					<div class="hyperglobe lg">
						<span></span>
						<span></span>
						<span></span>
						<span></span>
						<span></span>
						<span></span>
						<span></span>
						<span></span>
						<span></span>
						<span></span>
						<span></span>
						<span></span>
						<span></span>
						<span></span>
						<span></span>
						<span></span>
						<span></span>
						<span></span>
						<span></span>
						<span></span>
						<span></span>
					</div>
				</div>
				<div class="row">
					<div class="col-1-2">
						<h6>Ripple <span class="light">— A div with a class of <code>.ripple</code> with 1 empty <code>span</code></span></h6>
						<div class="hyper-animation">
							<div class="spinner"></div>
						</div>
					</div>
					<div class="col-1-2">
						<h6>Ripple <span class="light">— A div with a class of <code>.ripple</code> with 1 empty <code>span</code></span></h6>
						<div class="hyper-animation">
							<div class="ripple"><span></span></div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<?php include './assets/partials/footer.php'; ?>
	</div>
	<?php include './assets/partials/hyper/hypergrid.php'; ?>
</body>
<?php include './assets/partials/foot.php'; ?>